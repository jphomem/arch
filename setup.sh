# Script should be run after chrooting into the mounted root partition, after
# generation of the fstab file and after pacstrapping the following packages
# into the mountpoint: 

# base, base-devel, grub, efibootmgr, git, neovim, perl, luajit

# You can comment out the i ation of each group of packages, if you
# don't want that particular group; for example, we include graphics drivers
# for AMD, Intel, and NVidia, but you probably only need drivers for one GPU
# manufacturer. The script should be run as root.

# Time zone and localization configuration
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systoh --utc
echo -e "en_US.UTF-8 UTF-8 " | tee -a /etc/locale.gen
echo -e "LANG=en_US.UTF-8" | tee -a /etc/locale.conf
locale-gen

echo -e "[multilib]\nInclude = /etc/pacman.d/mirrorlist" | tee -a /etc/pacman.conf
pacman -Syu
function i {
pacman -S --noconfirm $*
}
i pacman-contrib lostfiles 

# Users setup
echo "Enter a password for root (the administrator user)."
passwd
useradd -mg users -G wheel,storage,power -s /bin/bash user
echo "Now enter a password for the unprivileged user."
passwd user
echo "%wheel ALL=(ALL) ALL" | tee -a /etc/sudoers

# Basic tools and terminal
i zsh python python2 lua dmenu ranger dialog ed htop neofetch cronie rsync grc
systemctl enable cronie.service

#/* 	Connectivity and Security
# *	Network connectivity, network protocols, web browsing and scraping,
# *	package downloading and building, and security tools.
# */
	#### Network Management
i networkmanager avahi ufw
ufw enable
systemctl enable ufw.service
systemctl enable NetworkManager.service
echo "konstantiniyye" | tee /etc/hostname
####
	#### Sneaky Secrets
i openssh pass gnupg ccrypt cfv sshguard libsecret tor
# (VPN support must be specifically configured posteriously)
#### 
	#### Web Utilities
i elinks irssi aria2 transmission-cli
####

#/* 	Technical Software
# * 	Compilers, interpreters, programming and technical applications.
# */
	#### Scripting
i bpython python-virtualenv python-pip tcl tk
mkdir /home/user/py
####
	#### Python2
i bpython2 python2-virtualenv python2-pip
####
	#### Compilers
i go cython jdk10-openjdk
####
	#### LAMP
i apache mariadb php php-apache
systemctl enable httpd.service
####
	#### Math
i openblas gap r sagemath python-numpy
####

#/* 	Display, GUI, and Desktop Environment
# *	GUI-based applications, desktop, and packages that depend on a visual
# *	environment.
# */
	#### Drivers and Display
i xf86-video-amdgpu # AMD
i xf86-video-nouveau # NVidia
i xf86-video-intel intel-ucode # Intel
i tlp xf86-input-libinput xf86-input-evdev xxkb
i xorg-server xorg-xinit mesa lib32-mesa
####
	#### Display Manager and Window Manager
i lightdm lightdm-gtk-greeter openbox lxsession-gtk3 compton
systemctl enable lightdm.service
cp /etc/X11/xinit/xinitrc /home/user/.xinitrc
echo -e "exec openbox-session" >> /home/user/.xinitrc
mkdir /home/user/.config
mkdir /home/user/.config/openbox
cp /etc/xdg/openbox/* /home/user/.config/openbox
####
	#### Configuration
i obconf lxappearance-obconf-gtk3 lightdm-gtk-greeter-settings
i arc-gtk-theme papirus-icon-theme xcursor-simpleandsoft breeze-gtk
i ttf-dejavu ttf-linux-libertine ttf-roboto tex-gyre-fonts
####
	#### Desktop Utilities
i rxvt-unicode obmenu tint2 nitrogen scrot gmrun network-manager-applet
i nemo gvfs ntfs-3g udisks2 udiskie
i pulseaudio pulseaudio-alsa pavucontrol alsa-utils flac
mkdir /home/user/Wallpapers
cp /arch/wallpaper.jpg /home/user/Wallpapers
####
	#### Fonts
i t1utils freetype2 fontconfig fontforge
i noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
####
	#### Graphical Utilities
i dbeaver geogebra transmission-gtk filezilla gufw gnome-keyring
i chromium pepper-flash mypaint gparted redshift gnome-maps
####

#/*	Media, Documents, and Office
# *	Codecs and applications for accessing media files, opening and creating
# *	documents, and general office operation.
# */
	#### Media
i eog eog-plugins leafpad file-roller gnome-mpv ghex
i audacious audiofile
####
	#### Documents
i evince texlive-most texlive-langextra texlive-langgreek biber texstudio
####
	#### Internet
i caprine youtube-dl
####
	#### Productivity
i gnucash cherrytree keepassxc kicad gimp inkscape electrum
i wine wine_gecko wine-mono
####

#### Configs
cp -r /arch/userHome/. /home/user
cp -r /arch/Laza /usr/share/themes
cp /arch/euler.otf /usr/share/fonts
chown -R user /home/user

echo "You will now be logged as the unprivileged user."
su user # the_end
