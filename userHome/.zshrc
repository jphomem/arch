export PS1="$ "

alias diff="diff --color=auto"
alias grep="grep --color=auto"
alias ls="ls --color=auto"

# [[ -s "/etc/grc.zsh" ]] && source /etc/grc.zsh
