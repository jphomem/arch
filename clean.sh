# The script should be run as an unprivileged user.

cd /home/user
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R user /home/user/yay
cd /home/user/yay
makepkg -si
cd /home/user
sudo rm -rf /home/user/yay
sudo mkdir /home/user/.config/yay
yay -S pa-applet-git gtk2-perl pacgraph pacman-contrib lostfiles localepurge
sudo rm -f /etc/locale.nopurge
echo -e "SHOWFREEDSPACE\nMANDELETE\nen_US.UTF-8" | sudo tee -a /etc/locale.nopurge
sudo localepurge
yay -R localepurge
sudo paccache -rk0
sudo systemctl enable paccache.timer
sudo rm -rf /arch /home/user/.git 
sudo rm -f /home/user/readme.md /home/user/setup.sh home/user/clean.sh

echo "Now, install grub, 'exit' the chrootjail, 'umount /mnt', and 'reboot'."
echo "The grub installation depends on whether you want to go for BIOS or UEFI,
and which device you want to install grub in (usually /dev/sda, but not always)."
echo "If you want to go for BIOS on the standard /dev/sda, run the commands:"
echo "sudo grub-mkconfig -o /boot/grub/grub.cfg"
echo "sudo grub-install /dev/sda"
echo "To see a list of devices, you can 'lsblk' ('list blocks')."